(function(){
	'use strict';

	var app = angular.module('myapp', ['ui.bootstrap', 'ui.router'])
	.config(function($stateProvider, $urlRouterProvider, $locationProvider){
		$locationProvider.html5Mode(true);

		$urlRouterProvider
            .otherwise('/');

        $stateProvider
	        .state('home', {	            
	            url: '/home',	            
	            views: {	                
	                'main': {
	                    templateUrl: 'views/includes/footer.html',
	                    controller: 'MainCtrl'
	                }
	            }
	        })
	})
})();